"""
Created on Thu Nov 18 14:20:28 2021

@author: Mélanie Geulin
"""

import sim as s
from MARK_4 import loop
from MARK_4 import loop_rob
from MARK_4 import intro
from MARK_4 import intro_rob

if __name__ == '__main__':
    
    s.simxFinish(-1) # just in case, close all opened connections
    clientID=s.simxStart('127.0.0.1',19997,True,True,5000,5) # Connect to CoppeliaSim
    if clientID!=-1:
        print ('Connected to remote API server')
        B,angle = intro()
        loop(clientID,B,angle) 
        
        # B,angle = intro_rob()
        # loop_rob(B,angle)

    else:
    	print ('Failed connecting to remote API server')
