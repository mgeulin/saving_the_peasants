# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 14:17:32 2021

@author: Mélanie Geulin
"""

import sim as s
import math as m
import numpy as np
import time
import matplotlib.pyplot as plt
import urx

###################################################################################################################################
N = 1000
xd = np.zeros((3,N))
xr = np.zeros((3,N))
xerr = np.zeros((3,N))
qd = np.zeros((6,N))
qr = np.zeros((6,N))
t_f = 15
qp = np.zeros((6,1))
joint_value = np.zeros((6,1))
qpn = np.zeros((6,1))
###################################################################################################################################

def DH(theta,alpha,a,r):
    nbre_joint = np.size(theta)
  
    result = np.zeros((nbre_joint,5)) #size of the matrice is 6 line and 5 column
    #CONFIGURE THETA
    for i in range (0,nbre_joint):
        result[i,3] = theta[i]
    result[:,3] =result[:,3] + [-np.pi/2,-np.pi/2,0,-np.pi/2,0,0]
    #CONFIGURE SIGMA
    for i in range (0,nbre_joint):
        result[i,0] = 0
       
    #CONFIGURE Alfa j-1
    result [:,1] = alpha
    #CONFIGURE A j-1
    result [:,2] = a
    #CONFIGURE r j-1
    result [:,4] = r
    
###############################################################
    # MODIFIED  DENAVIT HARTENBERG CONVENTION
    T = np.zeros((nbre_joint,4,4))
    DH = np.zeros((4,4))
    for i in range (0,nbre_joint):
        
        DH[0,0] = np.cos(result[i,3])
        DH[0,1] = -np.sin(result[i,3])
        DH[0,2] = 0
        DH[0,3] = result[i,2]
        
        DH[1,0] = np.cos(result[i,1]) * np.sin(result[i,3])
        DH[1,1] = np.cos(result[i,1]) * np.cos(result[i,3])
        DH[1,2] = -np.sin(result[i,1])
        DH[1,3] = -result[i,4] * np.sin(result[i,1])
        
        DH[2,0] = np.sin(result[i,1]) * np.sin(result[i,3])
        DH[2,1] = np.sin(result[i,1]) * np.cos(result[i,3])
        DH[2,2] = np.cos(result[i,1])
        DH[2,3] = result[i,4] * np.cos(result[i,1])
        
        DH[3,0] = 0
        DH[3,1] = 0
        DH[3,2] = 0
        DH[3,3] = 1
        
        T[i] = DH
    
    return T

def Transformed_Matrix(theta,alpha,a,r):
    T = DH(theta, alpha, a, r)
    T_hom = np.identity(4)
    nbre_joint = np.size(theta)

    for i in range (0,nbre_joint):
        T_hom = np.dot(T_hom,T[i])
    return T_hom

def Pose(theta,alpha,a,r):
    T_hom = Transformed_Matrix(theta,alpha,a,r)
    endeffector = np.array([0,0,0.0922,1])
    Pose = np.dot (T_hom, endeffector)
    return Pose
      
def rot(theta,alpha,a,r):
    T_hom = Transformed_Matrix(theta,alpha,a,r)
    R = T_hom[0:3,0:3]
    return R

def rpy_rot(RPY):
    R = np.zeros((3,3))
    rot_z = np.array([[ np.cos(RPY[0]) ,-np.sin(RPY[0]) ,0 ]  ,  [np.sin(RPY[0]),np.cos(RPY[0]),0]  ,  [0,0,1]  ])
    rot_y = np.array([[ np.cos(RPY[1]) ,  0, np.sin(RPY[1])]  ,  [0,1,0],  [-np.sin(RPY[0]),  0,  np.cos(RPY[0])]])
    rot_x = np.array([[ 1, 0, 0] , [0, np.cos(RPY[2]),-np.sin(RPY[2])]  ,  [0, np.sin(RPY[2]),np.cos(RPY[2])]   ])
    
    R = np.dot(rot_z,rot_y)
    R = np.dot(R,rot_x)
    return R

def angle_axis(Rot_init,Rot_fin):
    rot = np.transpose(Rot_init)
    rot = np.dot(rot,Rot_fin)
    cv = 1/2 * (rot[0,0]+rot[1,1]+rot[2,2]-1)
    sv = 1/2 * ((rot[1,2]-rot[2,1])**2 + (rot[2,0]-rot[0,2])**2 + (rot[0,1]-rot[1,0])**2 ) **0.5
    v = m.atan2(sv, cv)
    u = 1 / (2 * sv) * np.array([[rot[1,2]-rot[2,1]],[rot[2,0]-rot[0,2]],[rot[0,1]-rot[1,0]]])
    return u,v

def rot_uv(u,v):
    R = np.zeros((3,3))
    R[0,0] = u[0] * u[0] * (1 - np.cos(v)) + np.cos(v)
    R[0,1] = u[0] * u[1] * (1 - np.cos(v)) - u[2] * np.sin(v)
    R[0,2] = u[0] * u[2] * (1 - np.cos(v)) + u[1] * np.sin(v)
    R[1,0] = u[0] * u[1] * (1 - np.cos(v)) + u[2] * np.sin(v)
    R[1,1] = u[1] * u[1] * (1 - np.cos(v)) + np.cos(v)
    R[1,2] = u[1] * u[2] * (1 - np.cos(v)) - u[0] * np.sin(v)
    R[2,0] = u[0] * u[2] * (1 - np.cos(v)) - u[2] * np.sin(v)
    R[2,1] = u[1] * u[2] * (1 - np.cos(v)) + u[0] * np.sin(v)
    R[2,2] = u[2] * u[2] * (1 - np.cos(v)) + np.cos(v)
    return R

def Jacobian(theta,alpha,a,r):
    nbre_joint = np.size(theta)
    endeffector = np.array([0,0,0.0922,1])
    T = DH(theta,alpha,a,r)
    T_hom = Transformed_Matrix(theta,alpha,a,r)
            
        
    T10 = T[0]
    T20 = np.dot(T10, T[1])
    T30 = np.dot(T20, T[2])
    T40 = np.dot(T30, T[3])
    T50 = np.dot(T40, T[4])
    T60 = np.dot(T50, T[5])
    
    P6 = T60 [0:3,3]
    P5 = T50 [0:3,3]
    P4 = T40 [0:3,3]
    P3 = T30 [0:3,3]
    P2 = T20 [0:3,3]
    P1 = T10 [0:3,3]

    P16 = P6 - P1
    P26 = P6 - P2
    P36 = P6 - P3
    P46 = P6 - P4
    P56 = P6 - P5
    P66 = P6 - P6
    
    z_6 = T60 [0:3,2]
    z_5 = T50 [0:3,2]
    z_4 = T40 [0:3,2]
    z_3 = T30 [0:3,2]
    z_2 = T20 [0:3,2]
    z_1 = T10 [0:3,2]
    
    J60 = np.zeros((6,nbre_joint))
    
    P66 = np.cross(z_6,P66) 
    P56 = np.cross(z_5,P56) 
    P46 = np.cross(z_4,P46) 
    P36 = np.cross(z_3,P36) 
    P26 = np.cross(z_2,P26) 
    P16 = np.cross(z_1,P16) 
    
    J60[0:3,5] = P66
    J60[0:3,4] = P56
    J60[0:3,3] = P46
    J60[0:3,2] = P36
    J60[0:3,1] = P26
    J60[0:3,0] = P16
    
    J60[3:6,5] = z_6
    J60[3:6,4] = z_5
    J60[3:6,3] = z_4
    J60[3:6,2] = z_3
    J60[3:6,1] = z_2
    J60[3:6,0] = z_1
    
    I = np.identity(3)
    O = np.zeros((3,3))
    D = np.zeros((3,3))
    D[0,1] =  endeffector[2] * T_hom[2,2] 
    D[0,2] = - endeffector[2] * T_hom[1,2] 
    D[1,2] = + endeffector[2] * T_hom[0,2] 
    D[1,0] = - endeffector[2] * T_hom[2,2] 
    D[2,0] = + endeffector[2] * T_hom[1,2] 
    D[2,1] =  - endeffector[2] * T_hom[0,2] 
    
    Matrice = np.zeros((6,6))
    Matrice[0:3,0:3] = I
    Matrice[3:6,0:3] = O
    Matrice[3:6,3:6] = I
    Matrice[0:3,3:6] = D
    
    J = np.dot (Matrice,J60)
    return J

def ration(t,tf):
    r = 10 * (t/tf)**3 - 15 * (t/tf)**4 + 6 * (t/tf)**5
    return r

def Trajectory_pts(A,B,t,t_f):
    D = B - A
    r = ration (t,t_f)
    x = A + r * D
    return x

def Trajectory_angle_axis(Ri,u,v,t,t_f):
    rot_des = np.zeros((3,3))
    r = ration(t, t_f)
    rot_des = np.dot(Ri,rot_uv(u,r*v))
    return rot_des

def Trajectory_gene_q(q_i,q_f,t,t_f):
    D = np.add(q_i,-q_f)
    r = ration (t,t_f)
    x = q_i - r * D
    return x

def Get_joint_name(clientID,name):
    r,x = s.simxGetObjectHandle(clientID,name,s.simx_opmode_blocking)
    return x

def q_joint(clientID):
    #liste des joint dans mon robot
    Coppelia_joint_name = ("UR10_joint1","UR10_joint2","UR10_joint3","UR10_joint4","UR10_joint5","UR10_joint6")   
    Joint_name = np.zeros((6,1))
    q_load = np.zeros((6,1))
    #recuperer les index de ces joints de copellia et recipération des positions initial de chaque joint
    for i in range (0,len(Coppelia_joint_name)):
    	Joint_name[i] = Get_joint_name(clientID,Coppelia_joint_name[i])
    	rr,q_load[i] = s.simxGetJointPosition(clientID,int(Joint_name[i]),s.simx_opmode_streaming)

    return q_load,Joint_name    

def set_joint(clientID,qd,Joint_name):
    for i in range (0,6):
        s.simxSetJointTargetPosition(clientID,int(Joint_name[i]),qd[i],s.simx_opmode_streaming)
        
def take_time(t0,to):
    t = time.time() - to
    dt = t - t0
    t0 = t
    return t,t0,dt

def affiche(xd,xr,xerr,i):
    tt = np.arange(0,i,1)
    plt.plot(tt,xd[1][0:i])
    plt.plot(tt,xr[1][0:i])
    # plt.figure()
    # plt.plot(tt,xerr[1][0:i])
    
def intro():
    print("\nWelcome, User! Tell me, where do you want to go?")
    valx = input("Position in axe X: ")
    valy = input("Position in axe Y: ")
    valz = input("Position in axe Z: ")
    anglea = input("Terminal Organ alpha: ")
    angleb = input("Terminal Organ beta: ")
    angleg = input("Terminal Organ gamma: ")
    B = [valx,valy,valz]
    angle = [anglea,angleb,angleg]
    # B =  [1,0.5,0.2]
    return np.float32(B)*np.pi/180,np.float32(angle)*np.pi/180

def intro_rob():
    print("\nWelcome, User! Tell me, where do you want to go?")
    valx = input("X: ")
    valy = input("Y: ")
    valz = input("Z: ")
    anglea = input("RX: ")
    angleb = input("RY: ")
    angleg = input("RZ: ")
    B = [valx,valy,valz]
    angle = [anglea,angleb,angleg]
    # B =  [1,0.5,0.2]
    return np.float32(B)*np.pi/180,np.float32(angle)*np.pi/180
        
def loop(clientID,B,angle):
    s.simxStartSimulation(clientID, s.simx_opmode_oneshot)
   
    #DH initial parameters
    alpha = np.array([0,np.pi/2,0,0,np.pi/2,-np.pi/2])
    a = np.array([0,0,-0.612,-0.5723,0,0])
    r = np.array([0.1273,0.163941,0,0,0.1157,0])
    
    #Variables
    t0 = 0
    i = 0
    t = 0
    Kp = np.identity(3)
    Kp = 2
    s_reel = []
    acc_reel = []
    qp7 = []
    
    #initial state    
    q_load,Joint_name = q_joint(clientID)
    set_joint(clientID,[0.1,0.5,0.5,0,0,0],Joint_name)
    time.sleep(2)
    
    r,ot = s.simxGetObjectHandle(clientID,"UR10_joint6",s.simx_opmode_blocking)
    
    Pose_initial = Pose([0.1,0.5,0.5,0,0,0],alpha,a,r)
    A = Pose_initial[0:3]
     
    Rot_init = rot(q_load,alpha,a,r)  
    RPY = [0,0,np.pi/2]
    Rot_fin = rpy_rot(RPY)
    u,v = angle_axis(Rot_init,Rot_fin)
    to = time.time()
    
    while (t<t_f):
        t,t0,dt = take_time(t0,to)
        
        x_des =  Trajectory_pts(A,B,t,t_f)        
        q,j = q_joint(clientID)
       
        x_reel = Pose(q, alpha, a, r)
        
        rr,sp_reel,acce_reel = s.simxGetObjectVelocity(clientID, ot, s.simx_opmode_streaming)
        s_reel.append(np.sqrt(sp_reel[0]**2+sp_reel[1]**2+sp_reel[2]**2)*100)
        acc_reel.append(np.sqrt(acce_reel[0]**2+acce_reel[1]**2+acce_reel[2]**2)*100)
        
        x_err = x_des - x_reel[0:3]
        jac = Jacobian(q, alpha, a, r)
        inv_jac = np.linalg.pinv(jac[0:3,:])
        
        qpn [:,0] = np.dot(inv_jac,np.dot(Kp,x_err))
        qp = qpn[:]
        qp7.append(-qp[0,0])
        qd = (q +np.dot(qp,dt))
        set_joint(clientID,qd,Joint_name)
        xd[:,i] = x_des
        xr[:,i] = x_reel[0:3]
        xerr[:,i] = x_err
        i = i+1
    
    affiche(xd,xr,xerr,i)
    plt.figure()
    tt = np.arange(0,i,1)
    plt.plot(tt,qp7)
    plt.plot(tt,s_reel)
    plt.figure()
    plt.plot(tt,acc_reel)
    print(s_reel)
    
def loop_rob(B,angle):
    robot = urx.Robot("192.168.0.60")
   
    #DH initial parameters
    alpha_rob = np.array([0,0,0,0,0,-np.pi/2])
    a = np.array([0,0,-0.612,-0.5723,0,0])
    r = np.array([0.1273,0.163941,0,0,0.1157,0])
    
    #Variables
    t0 = 0
    i = 0
    t = 0
    Kp = np.identity(3)
    Kp = 5
    
    #initial state    
    q_load = robot.getj()
    time.sleep(2)
         
    Pose_initial = Pose(q_load,alpha_rob,a,r)
    A = Pose_initial[0:3]
     
    Rot_init = rot(q_load,alpha_rob,a,r)  
    RPY = angle
    Rot_fin = rpy_rot(RPY)
    u,v = angle_axis(Rot_init,Rot_fin)
    to = time.time()
    
    while (t<t_f):
        t,t0,dt = take_time(t0,to)
        
        x_des =  Trajectory_pts(A,B,t,t_f)        
        q = robot.getj()
       
        x_reel = Pose(q, alpha_rob, a, r)
                
        x_err = x_des - x_reel[0:3]
        jac = Jacobian(q, alpha_rob, a, r)
        inv_jac = np.linalg.pinv(jac[0:3,:])
        
        qpn [:,0] = np.dot(inv_jac,np.dot(Kp,x_err))
        qp = qpn[:]
        #qd = (q+np.dot(qp,dt))
        
        # robot.speedj([qp[0,0],qp[1,0],qp[2,0],qp[3,0],qp[4,0],qp[5,0]],acc = 1.5,min_time = 0.1)
        
        xd[:,i] = x_des
        xr[:,i] = x_reel[0:3]
        xerr[:,i] = x_err
        i = i+1
    
    affiche(xd,xr,xerr,i)
    
    
